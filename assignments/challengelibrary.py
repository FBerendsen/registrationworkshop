# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""
import matplotlib.pyplot as plt
import numpy as np
import registrationworkshop as rw
from mylibrarysolution import eulerTransform2D, eulerJacobian2DAt
from scipy.ndimage import gaussian_filter
import time
import os.path

def loadSnakes():    
    # read image from disk that serves as our 'fixed' image
    fixedImage = rw.readImage('image_A.npz')
    
    # read image from disk that serves as our 'moving' image
    movingImage = rw.readImage('image_B.npz')
        
    return fixedImage, movingImage

def loadTrainingImages(index):
    fixedImage = rw.readImage('training{0:d}fixedimage.npz'.format(index))
    movingImage = rw.readImage('training{0:d}movingimage.npz'.format(index))
    return fixedImage, movingImage

def loadTrainingPoints(index):
    fixedPoints = np.loadtxt('training{0:d}fixedpoints.txt'.format(index))
    movingPoints = np.loadtxt('training{0:d}movingpoints.txt'.format(index))
    return fixedPoints, movingPoints

def loadTestingImages(index):
    fixedImage = rw.readImage('testing{0:d}fixedimage.npz'.format(index))
    movingImage = rw.readImage('testing{0:d}movingimage.npz'.format(index))
    return fixedImage, movingImage

def loadTestingPoints(index):
    fixedPoints = np.loadtxt('testing{0:d}fixedpoints.txt'.format(index))
    movingPoints = None
    try:
        movingPoints = np.loadtxt('testing{0:d}movingpoints.txt'.format(index))
    except FileNotFoundError as err:
        print(err)
        print("Continuing... Moving points will only be available by the workshop organization")
        pass
    return fixedPoints, movingPoints

def runSubmissionExperiments(experimentFunction, teamName, myDescription):

    transformedFixedPointsTraining=[]
    timingTraining=[]
    transformedFixedPointsTesting=[]
    timingTesting=[]

    # Testing set
    for index in range(1,4):
        print("Testing set: {0:d}".format(index))
        fixedImage, movingImage = loadTrainingImages(index)
        fixedPoints, dummy = loadTestingPoints(index)
        elapsedTime=float('nan') # submitted value in case of failed registration
        transformedFixedPoints = np.empty_like(fixedPoints)
        transformedFixedPoints.fill(np.nan)# submitted value in case of failed registration
        try:
            startTime = time.time()
            transformFunction, finalParameters, centerOfRotation, metricTrajectory = experimentFunction(fixedImage,movingImage)
            elapsedTime = time.time() - startTime
            print("Elapsed time: {0:f}".format(elapsedTime))          
            transformedFixedPoints = transformFunction(fixedPoints,finalParameters,centerOfRotation)                    
        except Exception as err:
            print(err)
            print("Registration failed. This set will be excluded from submission.")
            input("Press Enter to continue.")
            pass
        timingTesting.append(elapsedTime)        
        transformedFixedPointsTesting.append(transformedFixedPoints)
     
    # Training set
    for index in range(1,4):
        print("Training set: {0:d}".format(index))
        fixedImage, movingImage = loadTrainingImages(index)
        fixedPoints, dummy = loadTrainingPoints(index)
        elapsedTime=float('nan') # submitted value in case of failed registration
        transformedFixedPoints = np.empty_like(fixedPoints)
        transformedFixedPoints.fill(np.nan)# submitted value in case of failed registration
        try:        
            startTime = time.time()
            transformFunction, finalParameters, centerOfRotation, metricTrajectory = experimentFunction(fixedImage,movingImage)
            elapsedTime = time.time() - startTime
            print("Elapsed time: {0:f}".format(elapsedTime))            
            transformedFixedPoints = transformFunction(fixedPoints,finalParameters,centerOfRotation)  
        except Exception as err:
            print(err)
            print("Registration failed. This set will be excluded from submission.")
            input("Press Enter to continue.")
            pass
        timingTraining.append(elapsedTime)        
        transformedFixedPointsTraining.append(transformedFixedPoints)

    # Benchmark
    print("TimingBenchmark")
    startTime = time.time()
    transformFunction, finalParameters, centerOfRotation, metricTrajectory = experimentTimingBenchmark(fixedImage,movingImage)
    benchmarkTime = time.time() - startTime
    print("benchmark time: {0:f}".format(benchmarkTime))
    
    np.savez( teamName+'.npz', teamName = teamName, 
                 description = myDescription,
                 transformedFixedPointsTraining = transformedFixedPointsTraining, 
                 timingTraining = timingTraining,
                 transformedFixedPointsTesting = transformedFixedPointsTesting, 
                 timingTesting = timingTesting,
                 benchmarkTime = benchmarkTime)
    return 

def experimentTimingBenchmark(fixedImage,movingImage):
    centerOfRotation = rw.getCenterOfImage(fixedImage)
    paramorigin = np.array([0,0,0],dtype='float')
    parameterScales = np.array([0.05,1,1])
    stepSize = 1e-7
    numberOfIterations = 3
    
    metricFunction = meanSquaredDifferenceWithGradient
    transformFunction = eulerTransform2D 
    transformJacobianFunction = eulerJacobian2DAt
    parameterUpdateFunction = constantStepUpdate(stepSize,parameterScales)
    
    metricTrajectory, parameterTrajectory = optimize(fixedImage, movingImage, metricFunction, transformFunction, transformJacobianFunction, paramorigin, parameterUpdateFunction, centerOfRotation, numberOfIterations)
    return transformFunction, parameterTrajectory[-1,:], centerOfRotation, metricTrajectory

    
def warpMovingImage(fixedImage, movingImage, transformFunction, parameters, centerOfRotation):
    warpedMovingImage = rw.createImage(**fixedImage)
    imageGrid = rw.imageGridToPoints(warpedMovingImage)
    warpedMovingImage['data'] = rw.imageInterpolator(movingImage, transformFunction(imageGrid,parameters,centerOfRotation))
    return warpedMovingImage

def fixedAndWarpedMovingDifferenceImage(fixedImage, movingImage, transformFunction, parameters, centerOfRotation):
    differenceImage = rw.createImage(**fixedImage)
    imageGrid = rw.imageGridToPoints(differenceImage)
    differenceImage['data'] = fixedImage['data'] - rw.imageInterpolator(movingImage, transformFunction(imageGrid,parameters,centerOfRotation))
    return differenceImage

def meanSquaredDifferenceWithGradient(fixedImage, movingImage, transformFunction, transformJacobianFunction, parameters, centerOfRotation):
    #dS/dmu = dS/dI * dI/dT * dT/dmu
    samplingGridAtFixed = rw.imageGridToPoints(fixedImage)
    transformedSamplingGrid = transformFunction(samplingGridAtFixed,parameters,centerOfRotation)
    intensityAndGradients, isValidPoint = rw.imageInterpolatorWithGradients(movingImage,transformedSamplingGrid, sparse=True)
    parameterGradients=np.zeros_like(parameters)
    fixedIntensities = fixedImage['data'][isValidPoint]
    validFixedPoints = samplingGridAtFixed[isValidPoint,:]

    numberOfValidpoints= intensityAndGradients.shape[0]
    for validPointIndex in range(numberOfValidpoints):
        fixedInt= fixedIntensities[validPointIndex]
        movingInt = intensityAndGradients[validPointIndex,0]
        movingGrad = intensityAndGradients[validPointIndex,1:]
        point=validFixedPoints[validPointIndex,:]
        parameterGradients+= -2.0*(fixedInt - movingInt) * movingGrad.dot( transformJacobianFunction(point, parameters, centerOfRotation) )

    parameterGradients/=np.sum(isValidPoint)
    metricValue = np.mean((fixedIntensities-intensityAndGradients[...,0])**2)
    return metricValue, parameterGradients

def constantStepUpdate(stepSize,parameterScales):
    def parameterUpdateFunction(parameters, parameterGradients, iterationNumber):
        parameters-=stepSize*parameterScales*parameterGradients
        return parameters
    return parameterUpdateFunction

def optimize(fixedImage, movingImage, metricFunction, transformFunction, transformJacobianFunction, initialParameters, parameterUpdateFunction, centerOfRotation, numberOfIterations):
    
    parameters = initialParameters
    #store optimization trajectory
    parameterTrajectory = np.empty((numberOfIterations,parameters.size),dtype=float)   
    metricTrajectory = np.empty((numberOfIterations,1),dtype=float)   
    for iterationNumber in range(numberOfIterations):
        
        metricValue, parameterGradients = metricFunction(fixedImage, movingImage, transformFunction, transformJacobianFunction, parameters, centerOfRotation)            
        
        parameterTrajectory[iterationNumber, :]= parameters
        metricTrajectory[iterationNumber, :] = metricValue
        
        parameters=parameterUpdateFunction(parameters,parameterGradients,iterationNumber)
        
        print('{0:d}: m={1:g}'.format(iterationNumber,metricValue))
    return metricTrajectory, parameterTrajectory
