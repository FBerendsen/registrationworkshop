# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""

# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 
# load python's plotting library 
import matplotlib.pyplot as plt

from mylibrary import eulerTransform2D

from mylibrary import meanSquaredDifferenceEulerWithGradient

import time

# read image from disk that serves as our 'fixed' image
fixedImage = rw.readImage('image_A.npz')

# read image from disk that serves as our 'moving' image
movingImage = rw.readImage('image_B.npz')

def optimizeEuler(fixedI, movingI, initialParameters, parameterScales, centerOfRotation,  stepsize = 5e-3, numberOfIterations=10):
    
    parameters = initialParameters
    #store optimization trajectory
    parameterTrajectory = np.empty((numberOfIterations,parameters.size),dtype=float)   
    metricTrajectory = np.empty((numberOfIterations,1),dtype=float)     
    for iterationNumber in range(numberOfIterations):
        
        # to be implemented
        # metricValue = ...        
        
        parameterTrajectory[iterationNumber, :]= parameters
        metricTrajectory[iterationNumber, :] = metricValue
        # parameters += ... 
        
        print('{0:d}: m={1:g}'.format(iterationNumber,metricValue))
    return metricTrajectory, parameterTrajectory



centerOfRotation = rw.getCenterOfImage(fixedImage)

paramorigin = np.array([0,0,0],dtype='float')

# tune your own parameterScales and stepsize
# parameterScales = 
# stepsize = 

starttime = time.time()
metricTrajectory, parameterTrajectory = optimizeEuler(fixedImage, movingImage, paramorigin, parameterScales, centerOfRotation, stepsize = stepsize, numberOfIterations=80)
elapsed_time = time.time() - starttime
print("elapsed time full sampling: {0:f}".format(elapsed_time))
print('the resulting parameters are:')
print(parameterTrajectory[-1,:])

optFig=plt.figure()
optAx = optFig.add_subplot(1,1,1)
optAx.plot(metricTrajectory)

registeredImage = rw.createImage(**fixedImage)
fixedImageGrid = rw.imageGridToPoints(fixedImage)

registeredImage['data'] = rw.imageInterpolator(movingImage, eulerTransform2D(fixedImageGrid,parameterTrajectory[-1,:],centerOfRotation))
rw.show_physical_image2D(registeredImage)

differenceInFixedImage = rw.createImage(**fixedImage)
differenceInFixedImage['data'] = fixedImage['data'] - registeredImage['data']
rw.show_physical_image2D(differenceInFixedImage)

