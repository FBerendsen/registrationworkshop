# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen
"""

# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 
# load python's plotting library 
import matplotlib.pyplot as plt

# load our previously defined meanSquaredDifferenceTranslationFiniteDifferenceGradient
from mylibrary import meanSquaredDifferenceTranslationFiniteDifferenceGradient

# read image from disk that serves as our 'fixed' image
fixedImage = rw.readImage('image_A.npz')

# read image from disk that serves as our 'moving' image
movingImage = rw.readImage('image_B.npz')

def optimizeTranslationFiniteDifference(fixedImage, movingImage, initialParameters, numberOfIterations):
    parameters = initialParameters
    #store optimization trajectory
    parameterTrajectory = np.empty((numberOfIterations,parameters.size),dtype=float)   
    metricTrajectory = np.empty((numberOfIterations,1),dtype=float)   
    #finite difference delta
    deltas = np.array([0.001,0.001])
    stepsize = 5e-3 
    for iterationNumber in range(numberOfIterations):
        
        metricValue, parameterGradients = meanSquaredDifferenceTranslationFiniteDifferenceGradient(fixedImage, movingImage, parameters ,deltas)
        
        parameterTrajectory[iterationNumber, :]= parameters
        metricTrajectory[iterationNumber, :] = metricValue
                
        parameters-=stepsize*parameterGradients
        
        print('{0:d}: m={1:g}, g={2}, p={3}'.format(iterationNumber,metricValue,parameterGradients, parameters))
        
    return metricTrajectory, parameterTrajectory


initialOffset = np.array([0.0,0.0])
fdMetricTrajectory, fdParameterTrajectory = optimizeTranslationFiniteDifference(fixedImage, movingImage, initialOffset, numberOfIterations=10)

# The file 'translationLandscape.npz' was calculated and saved to disk in assignment 3a
npzfile = np.load('translationLandscape.npz')
xsweep = npzfile['xsweep']
ysweep = npzfile['ysweep']
costlandscape = npzfile['costlandscape']
    
# plot the landscape as a mesh
landscapeFig = plt.figure()
landscapeAx = landscapeFig.add_subplot(1,1,1, projection='3d')
landscapeAx.plot_wireframe(xsweep[:,None],ysweep[None,:],costlandscape)

# plot the trajectory of the optimizer in the landscape figure
landscapeAx.plot(fdParameterTrajectory[:,0],fdParameterTrajectory[:,1],fdMetricTrajectory[:,0],'-*k')
landscapeAx.set_xlabel('x offset')
landscapeAx.set_ylabel('y offset')
landscapeAx.set_zlabel('cost')
