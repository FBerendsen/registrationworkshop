# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 11:55:16 2017

@author: FBerendsen
"""

# load the library made for this workshop
import registrationworkshop as rw
# load python's matrix library
import numpy as np 
# load python's plotting library 
import matplotlib.pyplot as plt

from challengelibrary import loadTrainingImages
from challengelibrary import loadTrainingPoints
from challengelibrary import loadTestingImages
from challengelibrary import meanSquaredDifferenceWithGradient
from challengelibrary import constantStepUpdate
from challengelibrary import optimize
from challengelibrary import fixedAndWarpedMovingDifferenceImage
from challengelibrary import runSubmissionExperiments

from mylibrary import eulerTransform2D, eulerJacobian2DAt


import time

def showTrainingSets():
    for index in [1,2,3]:
        print("Training set: {0:d}".format(index))
        # load the lung data
        fixedImage, movingImage = loadTrainingImages(index)
        fixedPoints, movingPoints = loadTrainingPoints(index)
        movingAx = rw.show_physical_image2D(movingImage)
        movingAx.plot(movingPoints[:,1],movingPoints[:,0],'sm')
        movingAx.set_title("Training {0:d}: moving image".format(index))
        movingAx.get_figure().savefig("training_{0:d}_moving.pdf".format(index),bbox_inches='tight')
        fixedAx = rw.show_physical_image2D(fixedImage)
        fixedAx.plot(fixedPoints[:,1],fixedPoints[:,0],'s',color='orange')
        fixedAx.set_title("Training {0:d}: fixed image".format(index))
        fixedAx.get_figure().savefig("training_{0:d}_fixed.pdf".format(index),bbox_inches='tight')

def runTrainingExperiments():
    TRETraining=[]
    timingListTraining = []
    # You can change next line into e.g. [1] to use training image 1 only
    for index in [1,2,3]:
        print("Training set: {0:d}".format(index))
        # load the lung data
        fixedImage, movingImage = loadTrainingImages(index)
        
        # The next block is where the registration takes place
        startTime = time.time()
        # You can change the name of the experiment called, but the input and 
        # output arguments should stay the same for the final testing procedure.
        transformFunction, finalParameters, centerOfRotation, metricTrajectory = experimentEulerMSD(fixedImage,movingImage)
        elapsedTime = time.time() - startTime
        print("Elapsed time: {0:f}".format(elapsedTime))
        timingListTraining.append(elapsedTime)
        fixedPoints, movingPoints = loadTrainingPoints(index)
        transformedFixedPoints = transformFunction(fixedPoints,finalParameters,centerOfRotation)    
        
        # The next blocks you can (un)comment or change to your preference
        
        # This optional block shows the warped moving image
        #warpedMovingImage = warpMovingImage(fixedImage, movingImage, transformFunction, finalParameters, centerOfRotation)
        #warpedMovingAx = rw.show_physical_image2D(warpedMovingImage)
        #warpedMovingAx.set_title("Training {0:d}: warped moving".format(index))
        
        # This optional block shows the difference image after registration
        differenceImage = fixedAndWarpedMovingDifferenceImage(fixedImage, movingImage, transformFunction, finalParameters, centerOfRotation)
        differenceAx = rw.show_physical_image2D(differenceImage)
        differenceAx.set_title("Training {0:d}: fixed - warped moving".format(index))
        
        # This optional block shows the moving image with the moving and transformed fixed landmarks 
        movingAx = rw.show_physical_image2D(movingImage)
        movingAx.plot(movingPoints[:,1],movingPoints[:,0],'sm',label='moving')
        movingAx.plot(transformedFixedPoints[:,1],transformedFixedPoints[:,0],'*',color='orange',label='transformed')
        movingAx.set_title("Training {0:d}: moving image with points".format(index))
        movingAx.legend()
        
        # This optional block shows the convergence plot of the registration
        metricFig = plt.figure()
        metricAx = metricFig.add_subplot(1,1,1)
        metricAx.plot(metricTrajectory)
        metricAx.set(title = "Training {0:d}: metric convergence".format(index), xlabel = 'iteration number', ylabel = 'metric value')
        
        TRE = np.sqrt(np.sum((transformedFixedPoints-movingPoints)**2,axis=1))
        print("Training {0:d}: TRE median: {1:.3f}, mean: {2:.3f} +/- {3:.3f}".format(index, np.median(TRE), np.mean(TRE),np.std(TRE)))
        TRETraining.append(TRE)

    TREoverall = np.concatenate(TRETraining)
    print("Overall: TRE median: {0:.3f}, mean: {1:.3f} +/- {2:.3f}".format(np.median(TREoverall), np.mean(TREoverall),np.std(TREoverall)))
    TREFig = plt.figure()
    TREAx = TREFig.add_subplot(1,1,1)
    TREAx.boxplot(TREoverall,positions=[0],widths=[0.6])
    TREAx.boxplot(TRETraining)
    TREAx.set(title = "TRE of all training sets", xlabel='overall and individual training pairs', xlim=[-0.5,3.5])
    


        
def experimentEulerMSD(fixedImage,movingImage):
    centerOfRotation = rw.getCenterOfImage(fixedImage)
    paramorigin = np.array([0,0,0],dtype='float')
    
    # tune your own parameterScales and stepsize
    parameterScales = np.array([0.05,1,1])
    stepSize = 1e-7
    numberOfIterations = 10
    
    metricFunction = meanSquaredDifferenceWithGradient
    transformFunction = eulerTransform2D 
    transformJacobianFunction = eulerJacobian2DAt
    parameterUpdateFunction = constantStepUpdate(stepSize,parameterScales)
    
    metricTrajectory, parameterTrajectory = optimize(fixedImage, movingImage, metricFunction, transformFunction, transformJacobianFunction, paramorigin, parameterUpdateFunction, centerOfRotation, numberOfIterations)
    return transformFunction, parameterTrajectory[-1,:], centerOfRotation, metricTrajectory

def experimentMyAdvancedMethod(fixedImage,movingImage):
    # implement this
    transformFunction, finalParameters, centerOfRotation = None
    return transformFunction, finalParameters, centerOfRotation 

# this performs the training
# runTrainingExperiments()

# this performs the registration on the testing set and saves the result in 'MyTeam.npz'
#myDescription = "Demo registration with Euler transform, mean squares difference metric and full sampling optimizer"
#runSubmissionExperiments(experimentEulerMSD,'MyTeam',myDescription)
