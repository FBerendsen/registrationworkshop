# -*- coding: utf-8 -*-
"""
NFBIA summer school registration workshop

@author: Floris Berendsen
"""

# load python's matrix library
import numpy as np 

# python basic structures can contain any type of data, e.g. integers, strings
# and even other objects:

#python list:
pythonList=[1,3.0,'ni']
#python tuple:
pythonTuple=(1,3.0,'ni')
#python dict (dictionary):
pythonDict={0:1,1:3.0,'last':'ni'}

# numpy arrays can contain only one type of data
numpyArray = np.array([1.0,3.14,2.72])
