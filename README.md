# README #

Workshop image registration implementation in python. Implement a basic registration algorithm in python. NFBIA summerschool Oktober 2017.

### What is this repository for? ###

* Assignments, documentation, library and scripts for the image registration implementation workshop
* Version 2.0

### How do I get set up? ###

####Requirements:####
* spyder: https://pythonhosted.org/spyder/

####Documentation####
* Assignment description: ImageRegistrationWorkshopAssignments.pdf 
* Reference tailored to this workshop: documents/miniReference.html
* presentation at NFBIA (based on VirtualMachine): implementation.odp

####Starting with the workshop:####
* start spyder
* open assignments/pythoninto.py
* open assignments/workshopinto.py
* open assignments/assignment1.py
* open assignments/assignment[...].py

### Who do I talk to? ###

* Floris Berendsen
* Hessam Sokooti
* Marius Staring
* Stefan Klein
* Josien Pluim
