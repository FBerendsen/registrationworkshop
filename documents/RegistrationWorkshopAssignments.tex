%=========================================================================
%
%  Copyright elastixteam and contributors
%
%  Licensed under the Apache License, Version 2.0 (the "License");
%  you may not use this file except in compliance with the License.
%  You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0.txt
%
%  Unless required by applicable law or agreed to in writing, software
%  distributed under the License is distributed on an "AS IS" BASIS,
%  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%  See the License for the specific language governing permissions and
%  limitations under the License.
%
%=========================================================================
% 
% NFBIA summer school registration workshop
%
% author: Floris Berendsen
%
\documentclass[a4paper]{article}
\usepackage[a4paper,inner=10mm,outer=60mm, top=20mm, bottom=40mm,footskip=.5cm,marginparwidth=50mm]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
%\usepackage{wrapfig}
%\usepackage{capt-of}
\usepackage{marginnote}
\usepackage{sidenotes}
\usepackage{enumitem}
\usepackage[T1]{fontenc} % for the guillemots

\usepackage{color, transparent}
\usepackage[percent]{overpic}
\usepackage{hyperref}
%\usepackage{titlesec}
%\titleformat{\section}
%  {\normalfont}{Assignment \thesection:}{1em}{}
\renewcommand\thesection{Assignment \arabic{section}:}
\renewcommand\thesubsection{\arabic{section}\alph{subsection}:}
	
\newcommand{\argmin}{\arg\!\min}
\author{F.F. Berendsen}
\title{Workshop Image Registration Implementation in Python}
\begin{document}
 \maketitle
 \section*{Introduction}

In this workshop you are going to program a basic 2-D registration algorithm. 
Programming will be done in the python language which runs in the virtual machine you received.
This document describes the assignments for the workshop. 
All workshop files can be found in: \texttt{/home/nfbia/Documents/registrationworkshop} or downloaded from \url{https://bitbucket.org/FBerendsen/registrationworkshop}.
Summary of important files for this workshop:
\begin{itemize}
\item \texttt{documents/miniReference.html} sheet: python, numpy and this workshop
\item \texttt{assignments/registrationworkshop.py}: library with interpolator and other basic functionality
\item \texttt{assignments/mylibrary.py}: collect your functions here
\item \texttt{assignments/challenge.py}: for afternoon's mini-challenge
\end{itemize}
Over the course of the assignments we will develop all elements that build up a registration algorithm. The final framework of the algorithm will look like figure \ref{fig:framework}.
\begin{figure}[htb]
\begin{center}
  \includegraphics[width=0.6\textwidth]{framework_gradient.pdf}
  \caption{Overview of the final registration framework} 
  \label{fig:framework}
 \end{center}
\end{figure}

 \section{translate an image (5 min)}
\textbf{Goal}: translate a moving image by an 4 mm in x-direction and -10 mm in y-direction.\\
\textbf{Steps}:
%\begin{enumerate}[label=\alph*)]
\begin{itemize}
\item Implement the function \texttt{translationtransform2D(points,offset)}, such that adds an \guillemotleft offset\guillemotright  to the input points.
\item Create a grid of points, called \texttt{fixedImageGrid}, resembling the fixed image pixel coordinates, using the function \text{rw.imageGridToPoints}.
\item Create a \texttt{transformedGrid} with offset \texttt{[4,-10]} by applying the \texttt{translationtransform2D} to the \texttt{fixedImageGrid}.
\item Use the resample function that you used in the previous exercise to interpolate the moving image at the \texttt{transformedGrid} position.
\item Use the \texttt{show\_physical\_image2D} function to plot the image.
\end{itemize}
%\end{enumerate}
\textbf{Question}: does the image move in the direction that you expected?

 \begin{itemize}
\item When your function \texttt{translationtransform2D(points,offset)} is ready don't forget to copy it to \texttt{mylibrary.py} 
\end{itemize}
\begin{marginfigure}
  \includegraphics[width=\textwidth]{solution1_1.pdf}
  \caption{Solution 1}
 \end{marginfigure}
 
 \section{rigid transform (5 min)}
 \textbf{Goal}: transform a moving image by an rigid transform.\\
  We define the rigid transform as an euler transform: 
  \[ T_{\mathrm{Euler}}(\mathbf{x};\mathbf{p}) = (\mathbf{x}-\mathbf{c}) \cdot A + \mathbf{c} + \mathbf{t}\]
  with $\mathbf{x}$ a point, $\mathbf{c}$ the center of rotation, matrix $A$ the rotational part of the transform and $\mathbf{t}$ the translational part of the transform. The parameters are stored in this order: 
	\[\mathbf{p} = \left[p_{0}, p_{1}, p_{2} \right], A = \begin{bmatrix}  \cos(p_{0}) & \sin(p_{0}) \\ -\sin(p_{0}) & \cos(p_{0}) \end{bmatrix}, \mathbf{t} = \begin{bmatrix}  p_{1} & p_{2}\end{bmatrix}\]

 \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution2_1.pdf}
  \caption{Solution 2}
 \end{marginfigure}

 \begin{itemize}
  \item Write the function \texttt{eulerTransform2D(points, parameters, centerOfRotation)} that transforms \texttt{points} (i.e. the \texttt{fixedImageGrid}).
  \item Create a \texttt{transformedGrid} with the \texttt{eulerParameters} provided in the assignment and resample the moving image.
  \item Show the image
  \item When your function \texttt{eulerTransform2D(points, parameters, centerOfRotation)} is ready copy it to \texttt{mylibrary.py}
 \end{itemize}
   
 \section{mean squared difference (15 min)}
 \textbf{Goal}: Calculate the similarity between a fixed image and a transformed moving image.
 
 \noindent In registration the similarity between images is optimized. In this workshop we use the mean squared difference between image as similarity metric, which gives a low value for images that are similar. 
  A mean squared difference, i.e. our cost function for registration, is defined: 
  \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution3a_1.pdf}
  \caption{\label{fig:solution3a}Solution 3a}
 \end{marginfigure}  
  \[C(F,M; \mathbf{p}) = \frac{1}{N}\sum_x{\left(F\left(\mathbf{x}\right) - M\left(T\left(\mathbf{x};\mathbf{p}\right)\right)\right)^2},\]
  with $F$ and $M$ the fixed and moving images, and $N$ the number of points.
 \subsection{translation transform}

 \begin{itemize}
 \item use the \texttt{translationTransform2D} and resample the moving image with a translation of $[-10,-25]$ into the fixed domain. Calculate the mean squared difference between the resampled image and the fixed image. The result should be 5044.2; \textbf{Hint} If you have \texttt{NaN} check the resampled imageData and think how to address Not-A-Numbers.
 \item implement the function \texttt{meanSquaredDifferenceTranslation(fixedImage, movingImage, offset)}
 \item run the script and plot the cost function as a landscape like in figure \ref{fig:solution3a}.
 \item copy \texttt{meanSquaredDifferenceTranslation(fixedImage, movingImage, offset)} to \texttt{mylibrary.py}
\end{itemize}

  \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution3b_1.pdf}
  \caption{\label{fig:solution3b}Solution 3b}
 \end{marginfigure}

\subsection{euler tranform }
\begin{itemize} 
 \item implement \texttt{meanSquaredDifferenceEuler}, i.e. for the euler transform
 \item create plots of each parameter sweeping over a range while keeping other parameters at the identity transform
\end{itemize}
\textbf{Question}: Can you explain why the shape of the cost function is so different when sweeping over the first parameter compared with the last two parameters?
\begin{itemize}
 \item copy \texttt{meanSquaredDifferenceEuler} to \texttt{mylibrary.py}
 \end{itemize}



\section{gradient descent optimization (15 min)}
\textbf{Goal}: Find the transform parameters automatically for optimal similarity between a fixed image and a transformed moving image.\\
 
 \begin{marginfigure}
  \begin{overpic}[width=\textwidth]{costfunctiongradients.pdf}
 \put (24,50) {$\mathbf{p}^k$}
 \put (28,56) {\color[rgb]{1,0,0} $-\frac{\partial C}{\partial p_0}$}
 \put (17,38) {\color[rgb]{1,0,0}$-\frac{\partial C}{\partial p_1}$}
 \put (33,25) {\color[rgb]{0,0,1}$-\frac{\partial C}{\partial \mathbf{p}}$}
 \put (50,0) {$p_0$}
 \put (0,40) {$p_1$}
\end{overpic}
  \caption{Gradient descent}
  \label{fig:direction}
\end{marginfigure}
\begin{marginfigure}
  \begin{overpic}[width=\textwidth]{costfunctionfinitedifferences.pdf}
 \put (24,60) {$p_0^k$}
 \put (24,48) {\color[rgb]{1,0,0}$\Delta_{p_0}$}
 \put (31,56) {\color[rgb]{1,0,0} $\Delta_C$}
 \put (56,58) {\color[rgb]{1,0,0}$\frac{\partial C}{\partial p_0} \approx \frac{\Delta_C}{\Delta_{p_0}}$}
 \put (57,11) {$p_1^k$}
 \put (47,18) {\color[rgb]{1,0,0} $\Delta_C$}
 \put (57,26) {\color[rgb]{1,0,0}$\Delta_{p_1}$}
 \put (15,27) {\color[rgb]{1,0,0}$\frac{\partial C}{\partial p_1} \approx \frac{\Delta_C}{\Delta_{p_1}}$}
 %\put (50,0) {$p_1$}
 %\put (50,38) {$p_0$}
\end{overpic}
  \caption{Gradient by finite differences, Solution 4a}
  \label{fig:finitedifference}
 \end{marginfigure}

 %\input{costfunctiongradients.pdf_tex}
 Registration is formulated as an optimization of the cost function:
 \[\argmin_\mathbf{p} C(\mathbf{p})\]
 Gradient descent is an iterative optimization technique that traverses the cost function landcape by following the steepest gradient direction. The process is formulated:
 \[ \mathbf{p}^{k+1} = \mathbf{p}^{k} + a^k \cdot \mathbf{d}^{k},\]
 with $k$ the iteration number, $a^k$: step size, and  $\mathbf{d}^k$: search direction.
 The search direction equals the negative partial derivative of the cost function with respect to the parameters: $\mathbf{d}^k = -\frac{\partial C}{\partial \mathbf{p}}(\mathbf{p}^k)$. This is illustrated by figures \ref{fig:direction} and \ref{fig:finitedifference}.
 \subsection{translation transform}
% \begin{marginfigure}
%  \includegraphics[width=0.45\textwidth]{solution4a_1.pdf}
%  \includegraphics[width=0.45\textwidth]{solution4a_2.pdf}
%  \caption{Solution 4a}
% \end{marginfigure}
% \begin{marginfigure}
%  \includegraphics[width=\textwidth]{solution4a_1.pdf}
%  \caption{Solution 4a}
% \end{marginfigure}
 \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution4a_2.pdf}
  \caption{Solution 4a}
 \end{marginfigure}
 \begin{itemize}
 \item the function \texttt{meanSquaredDifferenceTranslationFiniteDifferenceGradient} calculates the gradient of the cost function at position $\mathbf{p}^k$ by using a finite difference (\texttt{deltas}) on the parameters.
 \item run the script that plots the cost and gradient of the each parameter by sweeping over a range while keeping other parameters at the identity transform
 \item copy \texttt{meanSquaredDifferenceTranslationFiniteDifferenceGradient} to \texttt{mylibrary.py}
  \end{itemize}
\textbf{Question}: verify visually whether the plotted gradients are consistent with the cost function plot.
 
 \subsection{rigid transform}
%  \begin{marginfigure}
%  \includegraphics[width=\textwidth]{solution4b_1.pdf}
%  \caption{Solution 4b}
% \end{marginfigure}
 
% \begin{marginfigure} 
%  \includegraphics[width=\textwidth]{solution4b_2.pdf}
%  \caption{Solution 4b}
% \end{marginfigure}

 \begin{itemize} 
 \item this assignment is optional, but please do copy \texttt{meanSquaredDifferenceEulerFiniteDifferenceGradient} to \texttt{mylibrary.py}
 \item run the script that plots the cost and gradient of the each parameter by sweeping over a range while keeping other parameters at the identity transform
 \item choose the right \text{parameterscales} to normalize the search space
  \end{itemize}
  
  \subsection{optimize finite differences}
  \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution4c_1.pdf}
  \caption{Solution 4c}
 \end{marginfigure}

 \begin{itemize} 
 \item implement \texttt{optimizeTranslationFiniteDifference} that uses\\ \texttt{meanSquaredDifferenceTranslationFiniteDifferenceGradient}.
 \item run the optimization for 10 iterations
 \item plot the optimization trajectory in the cost function landscape figure of assignment 3a (it loads the landscape data that was generated when running assignment 3a, so run that at least once)
 \end{itemize}

\section{optimization by analytic gradients (45 min)}
\textbf{Goal}: improve the computational efficiency of optimization by using an analytically derived gradient of the cost function.\\

Instead of a finite difference we can derive the gradients of the cost function analytically. This involves using the chain rule.  
\[ \left.\frac{\partial C}{\partial \mathbf{p}}\right|_{\mathbf{p}=\mathbf{p}^k } = \left.\frac{\partial C}{\partial I}\right|_{I=M(T(\mathbf{x};\mathbf{p}^k))} \cdot \left.\frac{\partial M}{\partial \mathbf{y}}\right|_{\mathbf{y}=T(\mathbf{x};\mathbf{p}^k)} \cdot \left.\frac{\partial T}{\partial \mathbf{p}}\right|_{\mathbf{p}=\mathbf{p}^k }
 ,\]
 with:
   \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution5a_1.pdf}
  \caption{\label{fig:solution5a_1}Solution 5a}
 \end{marginfigure}
 \begin{marginfigure} 
  \includegraphics[width=\textwidth]{solution5a_2.pdf}
  \caption{\label{fig:solution5a_2}Solution 4b, 5a}
 \end{marginfigure}
  \begin{marginfigure} 
  \includegraphics[width=\textwidth]{solution5a_3.pdf}
  \caption{\label{fig:solution5a_3}Solution 4b, 5a}
 \end{marginfigure}
 \begin{itemize}
 \item $\left.\frac{\partial C}{\partial \mathbf{p}}\right|_{\mathbf{p}=\mathbf{p}^k} $ the partial derivative of the cost function $C$ with respect to its transform parameters $\mathbf{p}$, evaluated at the current parameter position $\mathbf{p}^k$.
 \item $\left.\frac{\partial C}{\partial I}\right|_{I=M(T(\mathbf{x};\mathbf{p}^k))}$ the partial derivative of the cost function with respect to the intensities of the moving image, evaluated at the current transformed moving image.
 \item $\left.\frac{\partial M}{\partial \mathbf{y}}\right|_{\mathbf{y}=T(\mathbf{x};\mathbf{p}^k)}$ the partial derivative of the intensities of the moving image with respect to spatial positions $\mathbf{y}$, evaluated at $\mathbf{y}$ being the current transformed points.
 \item $\left.\frac{\partial T}{\partial \mathbf{p}}\right|_{\mathbf{p}=\mathbf{p}^k }$ the partial derivative of the transformation with respect to the parameters, evaluated at the current parameter position $\mathbf{p}^k$.
 \end{itemize}
 
Writing out the chain rule for mean squared differences results in:
\begin{equation}
 %\frac{\partial C}{\partial \mathbf{p}} &= - \frac{2}{N}\sum_x{\left(F\left(\mathbf{x}\right) - M\left(\mathbf{T}\left(\mathbf{x};\mathbf{p}\right)\right)\right)}\frac{\partial M}{\partial \mathbf{p}}\\
 \underbrace{\frac{\partial C}{\partial \mathbf{p}}}_{\left[ 1 \times 3 \right]} = - \frac{2}{N}\sum_x{\underbrace{\left(F\left(\mathbf{x}\right) - M\left(\mathbf{T}\left(\mathbf{x};\mathbf{p}\right)\right)\right)}_{\left[ 1 \times 1 \right]}}\underbrace{\left[\frac{\partial M}{\partial \mathbf{y}}\right]}_{\left[ 1 \times 2 \right]}\underbrace{\left[\frac{\partial T}{\partial \mathbf{p}}\right]}_{\left[ 2 \times 3 \right]},\\
\label{eq:MSDderivative}
\end{equation}
with $\left[\frac{\partial M}{\partial \mathbf{y}}\right]$ the image gradient (row) vector and $\left[\frac{\partial T}{\partial \mathbf{p}}\right]$ the transformation Jacobian matrix and with the shapes of the matrices in case of a 2d Euler transform below the terms.

\subsection{analytic gradient mean squared difference}
\begin{itemize}
 \item transform and show an image like you did in assignment 2, except use \\ \texttt{rw.imageInterpolatorWithGradients} for interpolation. The calculated image gradients are the $\left[\frac{\partial M}{\partial \mathbf{y}}\right]$, as shown in figure \ref{fig:solution5a_1}.
 \item implement \texttt{eulerJacobian2DAt}: the partial derivative of the 2-dimensional euler transform $\left[\frac{\partial T}{\partial \mathbf{p}}\right] = \begin{bmatrix} \partial T_{x_0} / \partial \mathbf{p} \\ \partial T_{x_1} / \partial \mathbf{p} \end{bmatrix} = \begin{bmatrix} \partial T_{x_0} / \partial \mathbf{p}_0 & \partial T_{x_0} / \partial \mathbf{p}_1 & \partial T_{x_0} / \partial \mathbf{p}_2\\ \partial T_{x_1} / \partial \mathbf{p}_0 & \partial T_{x_1} / \partial \mathbf{p}_1 & \partial T_{x_1} / \partial \mathbf{p}_2 \end{bmatrix}$. Tip: \texttt{eulerJacobian2DAt} is a function for a single point $x$; the summation of equation \ref{eq:MSDderivative} will be implemented as a loop that includes \texttt{eulerJacobian2DAt}.
 \item implement the function \texttt{meanSquaredDifferenceEulerWithGradient} for the Euler transform, using the chain rule as explained.
 \item create cost and derivative plots of each parameter sweeping over a range while keeping other parameters at the identity transform
 \item verify visually that the analytic gradients are similar to the gradients calculated by finite differences
 \end{itemize}
\subsection{optimization of Euler parameters}
  \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution5b_1.pdf}
  \caption{\label{fig:solution5b_1}Solution 5b}
 \end{marginfigure}
  \begin{marginfigure}
     \includegraphics[width=\textwidth]{solution5b_2.pdf}
  \caption{\label{fig:solution5b_2}Solution 5b}
 \end{marginfigure}
  \begin{marginfigure}
  \includegraphics[width=\textwidth]{solution5b_3.pdf}
  \caption{\label{fig:solution5b_3}Solution 5b}
 \end{marginfigure}

\begin{itemize}
 \item implement gradient descent using the \texttt{meanSquaredDifferenceEulerWithGradient}
 \item run the optimization and tune the settings \texttt{parameterscales} and \texttt{stepsize} for a good convergence
 \item plot the cost function against the iteration number
 \item show the difference image of the the fixed image and the resampled moving image.
\end{itemize}

\section*{Mini-challenge}
This challenge is about improving the registration algorithm for realistic data as in figure \ref{fig:testdata}.
\begin{figure}[h!]
\includegraphics[height=2cm]{training_1_fixed.pdf}
\includegraphics[height=2cm]{training_1_moving.pdf}
\includegraphics[height=2cm]{training_2_fixed.pdf}
\includegraphics[height=2cm]{training_2_moving.pdf}
\includegraphics[height=2cm]{training_3_fixed.pdf}
\includegraphics[height=2cm]{training_3_moving.pdf}
\label{fig:testdata}
\caption{\label{fig:testdata}Training set of 3 pairs of fixed and moving images with manually annotated landmarks}
\end{figure} 
The performance of your improved the registration algorithm will be evaluated on the target registration error (TRE) measured between the landmarks.
During the development and tuning of you algorithm you can use the data of the training set.
At 10 min before the break you will get the testing data set consisting 3 pairs of images similar to training data on which you need to run your algorithm without adaptations. 
The result that you need to hand in (using our script) consists of:
\begin{itemize}
 \item a short description of your approach,
 \item the transformed landmarks (of both training and testing sets) on which we calculate the TREs,
 \item the run times (we benchmark your machine to measure a relative speed).
\end{itemize}
We will rank the submissions on median TRE of the 3 testing sets combined and pick 3 teams that will present their algorithms (no need to prepare slides, just talk a few minutes).
Your implementation should be pure python + numpy + scipy, i.e. do not use advanced registration libraries.

\subsection*{material}
\begin{itemize}
\item \texttt{challenge.py}: Write your registration method here as a function of \texttt{fixedImage} and \texttt{movingImage}. Adapt \texttt{runTrainingExperiments()} to try your method and visualize intermediate and final results. In this file an example registration method is given with a Euler transform and Mean squared difference metric using some alternative functions from \texttt{challengelibrary.py}.
\item \texttt{challengelibrary.py} contains: helper functions for the challenge as well as the extended functions \texttt{optimize()}, \texttt{meanSquaredDifferenceWithGradient()} and  \texttt{constantStepUpdate()}. These extended functions take as arguments the python functions that make up an registration algorithm. This facilitates experimenting with alternative implementations, but you are free to use the methods you created in the assignments too. Run \texttt{runSubmissionExperiments(myMethod,'myTeam','myDescription')} to generate the \texttt{myTeam.npz} file for submission.
\end{itemize}


\subsection*{potential approaches}
\begin{itemize}
 \item Implement a multi-resolution registration algorithm to improve the capture range. Have a look at \texttt{rw.blurimage(image)}
 \item Rotation and translation only might not be sufficient. You could replace the Euler transform by an Affine transform:
  \[ T_{\mathrm{Affine}}(\mathbf{x};\mathbf{p}) = (\mathbf{x}-\mathbf{c}) \cdot A + \mathbf{c} + \mathbf{t},\]
  \[\mathbf{p} = \left[p_{0}, p_{1}, p_{2}, p_{3}, p_{4}, p_{5}\right], A = \begin{bmatrix}  p_{0} & p_{1} \\ p_{2} & p_{3} \end{bmatrix}, \mathbf{t} = \begin{bmatrix}  p_{4} & p_{5}\end{bmatrix}\]
 \item Implement stochatic optimization by replacing the \texttt{imageGridToPoints} function by a random sub-sampling function. For random functions type \texttt{np.random} in the spyder's object explorer. Stochastic optimization requires a decaying step size such as $a^k = \frac{a_\mathrm{decay}}{\left(A_\mathrm{decay} +k +1 \right)^{\alpha_{\mathrm{decay}}}}$ typically with $A_\mathrm{decay} = 20$ and 
    $\alpha_{\mathrm{decay}} = 0.9 $.  
 \item Implement a cost function that calculates the normalized cross correlation between images. Relevant resource: \url{www.insight-journal.org/browse/publication/87}
\item There are various other modifications you can do. Be creative. 
 \end{itemize}

\end{document}
