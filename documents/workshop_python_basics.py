# -*- coding: utf-8 -*-
"""
/*=========================================================================
 *
 *  Copyright elastixteam and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 
NFBIA summer school registration workshop

@author: Floris Berendsen

python:
- short tutorial: http://www.stavros.io/tutorials/python/
- long tutorial: https://docs.python.org/2/tutorial/
- play without installing: https://repl.it/languages/python

numpy:
- tutorial: http://wiki.scipy.org/Tentative_NumPy_Tutorial
- for matlab users: http://wiki.scipy.org/NumPy_for_Matlab_Users
- cheat sheet (replace scipy by numpy): http://pages.physics.cornell.edu/~myers/teaching/ComputationalMethods/python/arrays.html
- easy to search reference: http://wiki.scipy.org/Numpy_Example_List_With_Doc
- about array broadcasting: http://wiki.scipy.org/EricsBroadcastingDoc
- advanced stuff: http://wiki.scipy.org/Cookbook/ViewsVsCopies

Try python inside a browser:
https://www.pythonanywhere.com/try-ipython/

"""





#function:
def modifyInputData(inputdata):
    inputdata[0]='modified'
    return inputdata
    
data=['original','data']
print(data)
outputdata = modifyInputData(data)
print(data)
print(outputdata)

data2=[0,1]
print(data2)
outputdata = modifyInputData(data2)
print(data2)
print(outputdata)


l1=[1,2,3]
for e in l1:
    e=4
print(l1)
l2=[[1],[2],[3]]
for e in l2:
    e[0]=4
print(l2)
#integer    

#float, i.e. 'float64'


#python list:
pythonList=[1,3.0,'ni']
#python tuple:
pythonTuple=(1,3.0,'ni')
#python dict (dictionary):
pythonDict={0:1,1:2,'last':'ni'}

import numpy as np
numpyArray = np.array([1.0,3.14,2.72])
numpyArray = np.array([1.0,3.14,2.72],dtype=bool)

#broadcasting (matlab bsxfun)
columnVector = np.array([[1],[2],[3]])
rowVector = np.array([[0.1,0.2,0.3]])
print(columnVector+rowVector)

# for loop, note indentation 
for element in pythonList:
    if element=='ni': 
        print(element)



xaxis = np.linspace(0,1,100)
yaxis = np.arange(0,1,0.1)

Xgrid, Ygrid = np.meshgrid(xaxis,yaxis)
Grid = np.meshgrid(xaxis,yaxis)
grid = np.array(np.meshgrid(xaxis,yaxis))


bigData=np.zeros((2,3,4,5,6))
bigData[0,1:2,:,:,:]

myData=np.empty((3,4,5))
for i0 in xrange(myData.shape[0]):
    for i1 in xrange(myData.shape[1]):
        for i2 in xrange(myData.shape[2]):
            myData[i0,i1,i2] = i0+0.1*i1+0.01*i2 # just some function


someData = np.arange(0.0,1.0,0.03)
result=[] # note: empty python list
for element in someData:
    result.append(element**2) # grow the list by one element
result=np.array(result)


#Pittfalls:
monoTuple=('singleElement',) 

#advanced stuff:

arglist=[0,1,3]
np.linspace(0,1,3)
np.linspace(*arglist)